# Fundamentos de Sistemas Embarcados: PROJETO 2 
Aluna: Giovana Vitor Dionisio Santana
Matricula: 180017659

## Instruções para compilação e execução
Neste repositório estão presentes os códigos para a execução do Servidor Central e do Servidor Distribuído.  

### Para executar:  
1. Clonar este repositório
2. Iniciar as duas instâncias dos servidores distribuídos (independem da ordem). Para isso:  
    2.1. Abrir a pasta _"distribuido"_  
    2.2. Executar, em um terminal, o comando ```make run``` e escolher no menu o andar correspondente ao servidor.  
    2.3 Executar, em outro terminal, o comando ```make run``` e escolher no menu o outro andar para o servidor.
3. Iniciar o Servidor Central, seguindo:  
    3.1. Abrir a pasta _"central"_  
    3.2. Executar, em um terminal, o comando ```make run```.

>OBS: Para realizar somente a compilação, utilize o comando ```make``` e para a deleção dos arquivos objetos e binário, utilize o comando ```make clean```

### Utilização:
#### Servidores Distribuídos
![img](https://i.ibb.co/whw7r5C/Captura-de-tela-de-2021-10-06-22-44-59.png)  
O seguinte menu é mostrado nas instâncias dos servidores distribuidos para a definição dos andares. Logo após ocorrer a conexao com o Servidor Central, é mostrada uma mensagem de confirmação ao usuário. 

#### Servidor Central
![img](https://i.ibb.co/Vmbt33q/Captura-de-tela-de-2021-10-06-22-38-08.png)  
O seguinte menu é mostrado para o Servidor Central. No box "GERAIS" são apresentados dados referentes ao pédio inteiro, no box "ESTADOS ANDAR TERREO" são mostrados dados sobre o andar térreo e no box "ESTADOS PRIMEIRO ANDAR" são mostrados dados sobre o primeiro andar. No box "MENU" é possível realizar a seleção do gerênciamento dos andares e do alarme. Basta digitar a opção escolhida e pressionar ENTER.
> Obs: a interface do menu pode levar alguns segundos para ser estabilizada; Para uma melhor experiência, utilize o terminal em tela cheia.
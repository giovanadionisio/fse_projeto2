#include "../inc/utils.h"

int menu_andar(){
    int escolha = -1;
    printf("Para utilizar este Servidor Distribuído, digite:\n");
    printf("1, caso o uso seja para o andar térreo;\n");
    printf("2, caso o uso seja para o primeiro andar;\n");

    while(1){
        scanf("%d", &escolha);
        if (escolha == 1 || escolha == 2){
            break;
        }
        printf("Valor inválido! Por favor, digite:\n");
        printf("1, caso o uso seja para o andar térreo;\n");
        printf("2, caso o uso seja para o primeiro andar;\n");
    }

    return escolha;
}

void le_arquivo(int andar){
    FILE *arq;
    char c;
    int cnt = 0;
    
    if(andar == 1){
        arq = fopen("config/configuracao_andar_terreo.json","r");
    } else {
        arq = fopen("config/configuracao_andar_1.json","r");
    }
    
    while(1){
        if(feof(arq)){
            break;
        }
        fscanf(arq, "%c", &c);
        arquivo_json[cnt++] = c;
    }
}


void finalizar_programa(int socketCentral, int socketDistribuido){
    printf("---- Servidor Central Desconectado ----");
    pthread_cancel(thread);
    pthread_cancel(t_ler_temperatura);  
    close(socketCentral);
    close(socketDistribuido);
    exit(0);
}

void tratasinal(int s){
    if (s == SIGPIPE || s == SIGINT){
        exit(0);
    }
}
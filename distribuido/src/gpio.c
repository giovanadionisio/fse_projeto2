#include "../inc/gpio.h"

void conf_inicial_gpio(){
    for(int i = 0; i < n_outputs; i++){
        pinMode(outputs_json[i].gpio, OUTPUT);
    }
}

void desligar_tudo(){
    for(int i = 0; i < n_outputs; i++){
        digitalWrite(outputs_json[i].gpio, LOW);
    }
}

void ligar_tudo(){
    for(int i = 0; i < n_outputs; i++){
        digitalWrite(outputs_json[i].gpio, HIGH);
    }
}

void ligar_lampada(int pin){
    digitalWrite(pin, HIGH);
    if(andar == 1){
        dispositivos_T.LC_T.estado = 1;
    } else {
        dispositivos_1.LC_1.estado = 1;
    }
}

void desligar_lampada(int pin){
    digitalWrite(pin, LOW);
    if(andar == 1){
        dispositivos_T.LC_T.estado = 0;
    } else {
        dispositivos_1.LC_1.estado = 0;
    }
}

void ligar_aspersor(){
    digitalWrite(16, HIGH);
    dispositivos_T.ASP.estado = 1;
}

void desligar_aspersor(){
    digitalWrite(16, LOW);
    dispositivos_T.ASP.estado = 0;
}

void muda_estado(int pin){
    digitalWrite(pin, !digitalRead(pin));
    if(andar == 1){
        switch (pin){
            case 4:
                dispositivos_T.LS_T01.estado = !dispositivos_T.LS_T01.estado;
                break;
            case 17:
                dispositivos_T.LS_T02.estado = !dispositivos_T.LS_T02.estado;
                break;
            case 27:
                dispositivos_T.LC_T.estado = !dispositivos_T.LC_T.estado;
                break;
            case 7:
                dispositivos_T.AC_T.estado = !dispositivos_T.AC_T.estado;
                break;
            case 16:
                dispositivos_T.ASP.estado = !dispositivos_T.ASP.estado;
                break; 
        }   
    } else {
        switch (pin){
            case 22:
                dispositivos_1.LS_101.estado = !dispositivos_1.LS_101.estado;
                break;
            case 25:
                dispositivos_1.LS_102.estado = !dispositivos_1.LS_102.estado;
                break;
            case 8:
                dispositivos_1.LC_1.estado   = !dispositivos_1.LC_1.estado;
                break;
            case 12:
                dispositivos_1.AC_1.estado   = !dispositivos_1.AC_1.estado;
                break; 
        }
    }
}

void handle_entrada(void){
    struct timeval now;
    unsigned long diff;

    gettimeofday(&now, NULL);

    diff = (now.tv_sec * 1000000 + now.tv_usec) - (last_change_entrada.tv_sec * 1000000 + last_change_entrada.tv_usec);
    if(diff > IGNORE_CHANGE_BELOW_USEC_C){
        if(!state_entrada) {
            n_pessoas++;
        }

        state_entrada = !state_entrada;
    }

    last_change_entrada = now;
}

void handle_saida(void){
    struct timeval now;
    unsigned long diff;

    gettimeofday(&now, NULL);

    diff = (now.tv_sec * 1000000 + now.tv_usec) - (last_change_saida.tv_sec * 1000000 + last_change_saida.tv_usec);
    if(diff > IGNORE_CHANGE_BELOW_USEC_C){
        if(!state_saida) {
            n_pessoas--;
        }

        state_saida = !state_saida;
    }

    last_change_saida = now;
}

void inicia_contagem_pessoas(){
    for (int i = 0; i < n_inputs; i++){
        if(!strcmp(inputs_json[i].tag, "SC_IN")){
            pin_entrada = inputs_json[i].gpio;
        } else if (!strcmp(inputs_json[i].tag, "SC_OUT")){
            pin_saida = inputs_json[i].gpio;
        }
    }

    gettimeofday(&last_change_entrada, NULL);
    gettimeofday(&last_change_saida, NULL);
    wiringPiISR(pin_entrada, INT_EDGE_BOTH, &handle_entrada);
    wiringPiISR(pin_saida, INT_EDGE_BOTH, &handle_saida);
    state_entrada = digitalRead(pin_entrada);
    state_saida = digitalRead(pin_entrada);
}

void handle_presenca(){
    struct timeval now;
    unsigned long diff;

    gettimeofday(&now, NULL);

    diff = (now.tv_sec * 1000000 + now.tv_usec) - (last_change_presenca.tv_sec * 1000000 + last_change_presenca.tv_usec);
    if(diff > IGNORE_CHANGE_BELOW_USEC){
        if(state_presenca) {
            if(andar == 1){
                dispositivos_T.SP_T.estado = 0;
            } else {
                dispositivos_1.SP_1.estado = 0;
            }
        } else {
            if(andar == 1){
                dispositivos_T.SP_T.estado = 1;
            } else {
                dispositivos_1.SP_1.estado = 1;
            }
        }

        state_presenca = !state_presenca;
    }

    last_change_presenca = now;
}

void handle_janela_1(){
    struct timeval now;
    unsigned long diff;

    gettimeofday(&now, NULL);

    diff = (now.tv_sec * 1000000 + now.tv_usec) - (last_change_janela_1.tv_sec * 1000000 + last_change_janela_1.tv_usec);
    if(diff > IGNORE_CHANGE_BELOW_USEC){
        if(state_janela_1) {
            if(andar == 1){
                dispositivos_T.SJ_T01.estado = 0;
            } else {
                dispositivos_1.SJ_1.estado = 0;
            }
        } else {
            if(andar == 1){
                dispositivos_T.SJ_T01.estado = 1;
            } else {
                dispositivos_1.SJ_1.estado = 1;
            }
        }

        state_janela_1 = !state_janela_1;
    }

    last_change_janela_1 = now;
}

void handle_janela_2(){
    struct timeval now;
    unsigned long diff;

    gettimeofday(&now, NULL);

    diff = (now.tv_sec * 1000000 + now.tv_usec) - (last_change_janela_2.tv_sec * 1000000 + last_change_janela_2.tv_usec);
    if(diff > IGNORE_CHANGE_BELOW_USEC){
        if(state_janela_2) {
            if(andar == 1){
                dispositivos_T.SJ_T02.estado = 0;
            } else {
                dispositivos_1.SJ_2.estado = 0;
            }
        } else {
            if(andar == 1){
                dispositivos_T.SJ_T02.estado = 1;
            } else {
                dispositivos_1.SJ_2.estado = 1;
            }
        }

        state_janela_2 = !state_janela_2;
    }

    last_change_janela_2 = now;
}

void handle_fumaca(){
    struct timeval now;
    unsigned long diff;

    gettimeofday(&now, NULL);

    diff = (now.tv_sec * 1000000 + now.tv_usec) - (last_change_fumaca.tv_sec * 1000000 + last_change_fumaca.tv_usec);
    if(diff > IGNORE_CHANGE_BELOW_USEC){
        if(state_fumaca) {
            if(andar == 1){
                dispositivos_T.SF_T.estado = 0;
            } else {
                dispositivos_1.SF_1.estado = 0;
            }
        } else {
            if(andar == 1){
                dispositivos_T.SF_T.estado = 1;
            } else {
                dispositivos_1.SF_1.estado = 1;
            }
        }

        state_fumaca = !state_fumaca;
    }

    last_change_presenca = now;
}

void handle_porta_entrada(){
    struct timeval now;
    unsigned long diff;

    gettimeofday(&now, NULL);

    diff = (now.tv_sec * 1000000 + now.tv_usec) - (last_change_porta_entrada.tv_sec * 1000000 + last_change_porta_entrada.tv_usec);
    if(diff > IGNORE_CHANGE_BELOW_USEC){
        if(state_porta_entrada) {
            dispositivos_T.SPo_T.estado = 0;
        } else {
            dispositivos_T.SPo_T.estado = 1;
        }

        state_porta_entrada = !state_porta_entrada;
    }

    last_change_porta_entrada = now;
}

void gerencia_monitoramento(){
    for(int i = 0; i < n_inputs; i++){
        if(!strcmp(inputs_json[i].tag, "SPo_T")){
            gettimeofday(&last_change_porta_entrada, NULL);
            wiringPiISR(inputs_json[i].gpio, INT_EDGE_BOTH, &handle_porta_entrada);
            state_porta_entrada = digitalRead(inputs_json[i].gpio);
            if(state_porta_entrada){
                dispositivos_T.SPo_T.estado = 1;
            }
        } else if (!strcmp(inputs_json[i].tag, "SJ_T01") || !strcmp(inputs_json[i].tag, "SJ_1")){
            gettimeofday(&last_change_janela_1, NULL);
            wiringPiISR(inputs_json[i].gpio, INT_EDGE_BOTH, &handle_janela_1);
            state_janela_1 = digitalRead(inputs_json[i].gpio);
            if(state_janela_1){
                if(andar == 1){
                    dispositivos_T.SJ_T01.estado = 1;
                } else {
                    dispositivos_1.SJ_1.estado = 1;
                }
            }
        } else if (!strcmp(inputs_json[i].tag, "SJ_T02") || !strcmp(inputs_json[i].tag, "SJ_2")){
            gettimeofday(&last_change_janela_2, NULL);
            wiringPiISR(inputs_json[i].gpio, INT_EDGE_BOTH, &handle_janela_2);
            state_janela_2 = digitalRead(inputs_json[i].gpio);
            if(state_janela_2){
                if(andar == 1){
                    dispositivos_T.SJ_T02.estado = 1;
                } else {
                    dispositivos_1.SJ_2.estado = 1;
                }
            }
        } else if (!strcmp(inputs_json[i].tag, "SP_T") || !strcmp(inputs_json[i].tag, "SP_1")){
            gettimeofday(&last_change_presenca, NULL);
            wiringPiISR(inputs_json[i].gpio, INT_EDGE_BOTH, &handle_presenca);
            state_presenca = digitalRead(inputs_json[i].gpio);
            if(state_presenca){
                if(andar == 1){
                    dispositivos_T.SP_T.estado = 1;
                } else {
                    dispositivos_1.SP_1.estado = 1;
                }
            }
        } else if (!strcmp(inputs_json[i].tag, "SF_T") || !strcmp(inputs_json[i].tag, "SF_1")){
            gettimeofday(&last_change_fumaca, NULL);
            wiringPiISR(inputs_json[i].gpio, INT_EDGE_BOTH, &handle_fumaca);
            state_fumaca = digitalRead(inputs_json[i].gpio);
            if(state_fumaca){
                if(andar == 1){
                    dispositivos_T.SF_T.estado = 1;
                } else {
                    dispositivos_1.SF_1.estado = 1;
                }
            }
        }
    }
}

void handle_notificacao(char notificacao[]){
    switch (notificacao[0]){
        case 50:
            ligar_lampada(27);
            break;
        case 51:
            desligar_lampada(27);
            break;
        case 52:
            ligar_lampada(8);
            break;
        case 53:
            desligar_lampada(8);
            break;
        case 60:
            ligar_aspersor();
            break;
        case 61:
            desligar_aspersor();
            break;
        default:
            muda_estado(notificacao[0]);
            break;
    }   
}
#include "../inc/socket.h"

void initSocket() {
    // create a Socket 
    if ((socketDistribuido = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        printf("Error when start socket. Try again\n");
        exit(1);
    }

    memset(&endDistribuido, 0, sizeof(endDistribuido));
	endDistribuido.sin_family = AF_INET;
	endDistribuido.sin_addr.s_addr = htonl(INADDR_ANY);
	endDistribuido.sin_port = htons(dados_json.porta_distribuido);

    // Bind
	if(bind(socketDistribuido, (struct sockaddr *) &endDistribuido, sizeof(endDistribuido)) < 0) {
		printf("Failed tried bind. Try again later!\n");
        exit(1);
    }

    if (listen(socketDistribuido, NUMBER_QUEUE) < 0) {
        printf("Failed in listening\n");
        exit(1);
    }

    printf("SERVIDOR DISTRIBUIDO INICIADO\n");

}

void *listenSocket_T() {
    int r;
    char notificacao[1];
    char pin_notificacao[1];
    tamEndereco = sizeof(endCentral);

    if ((socketCentral = accept(socketDistribuido, (struct sockaddr *) &endCentral, &tamEndereco)) < 0) {
        printf("Failed in accept\n");    
    } else {
        printf("==== Servidor Central Conectado ao Térreo! ====\n");
    }
    
    s_buffer = (char*) malloc(21*sizeof(char));
    r_buffer = (char*) malloc(21*sizeof(char));
    
    while(1) {
        r = read(socketCentral, notificacao, 1);
        if(r == -1){
            finalizar_programa(socketCentral, socketDistribuido);
        }
        if(notificacao[0] == 1){
            //printf("Chegou Notificação!\n");
            read(socketCentral, pin_notificacao, 1);
            if(r == -1){
                finalizar_programa(socketCentral, socketDistribuido);
            }
            handle_notificacao(pin_notificacao);
        }
        read(socketCentral, r_buffer, 21);
        if(r == -1){
            finalizar_programa(socketCentral, socketDistribuido);
        }
        mensagem_t();
        send(socketCentral, s_buffer, 21, 0);
    }
}

void *listenSocket_1() {
    int r;
    char notificacao[1];
    char pin_notificacao[1];
    tamEndereco = sizeof(endCentral);

    if ((socketCentral = accept(socketDistribuido, (struct sockaddr *) &endCentral, &tamEndereco)) < 0) {
        printf("Failed in accept\n");    
    } else {
        printf("==== Servidor Central Conectado ao Primeiro Andar! ====\n");
    }
    
    s_buffer = (char*) malloc(16*sizeof(char));
    r_buffer = (char*) malloc(16*sizeof(char));
    
    while(1) {
        read(socketCentral, notificacao, 1);
        if(r == -1){
            finalizar_programa(socketCentral, socketDistribuido);
        }
        if(notificacao[0] == 1){
            read(socketCentral, pin_notificacao, 1);
            if(r == -1){
                finalizar_programa(socketCentral, socketDistribuido);
            }
            handle_notificacao(pin_notificacao);
        }
        read(socketCentral, r_buffer, 16);
        if(r == -1){
            finalizar_programa(socketCentral, socketDistribuido);
        }
        mensagem_1();
        send(socketCentral, s_buffer, 16, 0);
    }
}

void mensagem_t(){
    s_buffer[0] = dispositivos_T.LS_T01.estado;
    s_buffer[1] = dispositivos_T.LS_T02.estado;
    s_buffer[2] = dispositivos_T.LC_T.estado;
    s_buffer[3] = dispositivos_T.AC_T.estado;
    s_buffer[4] = dispositivos_T.ASP.estado;
    s_buffer[5] = dispositivos_T.SP_T.estado;
    s_buffer[6] = dispositivos_T.SF_T.estado;
    s_buffer[7] = dispositivos_T.SJ_T01.estado;
    s_buffer[8] = dispositivos_T.SJ_T02.estado;
    s_buffer[9] = dispositivos_T.SPo_T.estado;
    s_buffer[10] = dispositivos_T.SC_IN.estado;
    s_buffer[11] = dispositivos_T.SC_OUT.estado;
    memcpy(&s_buffer[12], &temperatura, 4);
    memcpy(&s_buffer[16], &umidade, 4);
    s_buffer[20] = n_pessoas;
}

void mensagem_1(){
    s_buffer[0] = dispositivos_1.LS_101.estado;
    s_buffer[1] = dispositivos_1.LS_102.estado;
    s_buffer[2] = dispositivos_1.LC_1.estado;
    s_buffer[3] = dispositivos_1.AC_1.estado;
    s_buffer[4] = dispositivos_1.SP_1.estado;
    s_buffer[5] = dispositivos_1.SF_1.estado;
    s_buffer[6] = dispositivos_1.SJ_1.estado;
    s_buffer[7] = dispositivos_1.SJ_2.estado;
    memcpy(&s_buffer[8], &temperatura, 4);
    memcpy(&s_buffer[12], &umidade, 4);
}


#include "../inc/main.h"

void parse_json(){
    cJSON *root = cJSON_Parse(arquivo_json);
    cJSON *ip_central = cJSON_GetObjectItem(root, "ip_central");
    cJSON *porta_central = cJSON_GetObjectItem(root, "porta_central");
    cJSON *porta_distribuido = cJSON_GetObjectItem(root, "porta_distribuido");
    cJSON *nome = cJSON_GetObjectItem(root, "nome");
    cJSON *gpio_temperaura = cJSON_GetObjectItem(root, "gpio_temperatura");

    cJSON *outputs = cJSON_GetObjectItem(root, "outputs");
    n_outputs = cJSON_GetArraySize(outputs);

    cJSON *inputs = cJSON_GetObjectItem(root, "inputs");
    n_inputs = cJSON_GetArraySize(inputs);

    strcpy(dados_json.ip_central, ip_central->valuestring);
    dados_json.porta_central = porta_central->valueint;
    dados_json.porta_distribuido = porta_distribuido->valueint;
    strcpy(dados_json.nome, nome->valuestring);
    pin_temperatura = gpio_temperaura->valueint;

    for(int i = 0; i < n_outputs; i++){
        cJSON *object = cJSON_GetArrayItem(outputs,i);

        cJSON *output_type = cJSON_GetObjectItem(object, "type");
        cJSON *output_tag = cJSON_GetObjectItem(object, "tag");
        cJSON *output_gpio = cJSON_GetObjectItem(object, "gpio");
    
        strcpy(outputs_json[i].type, output_type->valuestring);
        strcpy(outputs_json[i].tag, output_tag->valuestring);
        outputs_json[i].gpio = output_gpio->valueint;
    }

    for(int i = 0; i < n_inputs; i++){
        cJSON *object = cJSON_GetArrayItem(inputs,i);

        cJSON *input_type = cJSON_GetObjectItem(object, "type");
        cJSON *input_tag = cJSON_GetObjectItem(object, "tag");
        cJSON *input_gpio = cJSON_GetObjectItem(object, "gpio");
    
        strcpy(inputs_json[i].type, input_type->valuestring);
        strcpy(inputs_json[i].tag, input_tag->valuestring);
        inputs_json[i].gpio = input_gpio->valueint;
    }

    cJSON_Delete(root);
}

void configuracao_arquivo(){
    andar = menu_andar();
    le_arquivo(andar);
    parse_json();
}

iniciar_variaveis(){
    if(andar == 1){
        dispositivos_T.LS_T01.estado = 0;
        dispositivos_T.LS_T02.estado = 0;
        dispositivos_T.LC_T.estado = 0;
        dispositivos_T.AC_T.estado = 0;
        dispositivos_T.ASP.estado = 0;
        dispositivos_T.SP_T.estado = 0;
        dispositivos_T.SF_T.estado = 0;
        dispositivos_T.SJ_T01.estado = 0;
        dispositivos_T.SJ_T02.estado = 0;
        dispositivos_T.SPo_T.estado = 0;
        dispositivos_T.SC_IN.estado = 0;
        dispositivos_T.SC_OUT.estado = 0;
        n_pessoas = 0;
    } else {
        dispositivos_1.LS_101.estado = 0;
        dispositivos_1.LS_102.estado = 0;
        dispositivos_1.LC_1.estado = 0;
        dispositivos_1.AC_1.estado = 0;
        dispositivos_1.SP_1.estado = 0;
        dispositivos_1.SF_1.estado = 0;
        dispositivos_1.SJ_1.estado = 0;
        dispositivos_1.SJ_2.estado = 0;
    }
    
    temperatura = 0.0;
    umidade = 0.0;
}

void iniciar_servidor(){
    initSocket();
    iniciar_variaveis();
    wiringPiSetup();
    wiringPiSetupGpio();
    conf_inicial_gpio();
    desligar_tudo();
    if(andar == 1){
        inicia_contagem_pessoas();
    }
    gerencia_monitoramento();
    //pthread_create(&ler_temperatura, NULL, &t_ler_temperatura, NULL);
    //pthread_join(ler_temperatura, NULL); 
}

int main(){
    signal(SIGPIPE, tratasinal);
    signal(SIGINT, tratasinal);

    configuracao_arquivo();
    iniciar_servidor();

    if(andar == 1){
        pthread_create(&thread, NULL, &listenSocket_T, NULL);
    } else {
        pthread_create(&thread, NULL, &listenSocket_1, NULL);
    }
    
    pthread_create(&t_ler_temperatura, NULL, &ler_temperatura, NULL);
    pthread_join(thread, NULL);
    pthread_join(t_ler_temperatura, NULL);

    return 0;
}
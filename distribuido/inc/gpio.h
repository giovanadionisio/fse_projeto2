#include <wiringPi.h>
#include <softPwm.h>
#include <string.h>
#include <stdio.h>
#include <sys/time.h>
#include "socket.h"

typedef struct{
    char type[30];
    char tag[30];
    int gpio;
} output;

typedef struct{
    char type[30];
    char tag[30];
    int gpio;
} input;

void conf_inicial_gpio();
void desligar_tudo();
void ligar_tudo();
void inicia_contagem_pessoas();
void gerencia_monitoramento();

int andar;

output outputs_json[5];
int n_outputs;
input inputs_json [7];
int n_inputs;

int pin_entrada, pin_saida;
int n_pessoas;

static volatile int state_entrada, state_saida, state_presenca, state_janela_1, state_janela_2, state_fumaca, state_porta_entrada;
struct timeval last_change_entrada;
struct timeval last_change_saida;
struct timeval last_change_presenca;
struct timeval last_change_janela_1;
struct timeval last_change_janela_2;
struct timeval last_change_fumaca;
struct timeval last_change_porta_entrada;

#define PIN 23
#define IGNORE_CHANGE_BELOW_USEC 50000
#define IGNORE_CHANGE_BELOW_USEC_C 30000
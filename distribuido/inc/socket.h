#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include "utils.h"

#define MAX 512
#define NUMBER_QUEUE 20

typedef struct {
    char ip_central[15];
    int porta_central;
    int porta_distribuido;
    char nome[15];
} dados_server;

dados_server dados_json;

int socketDistribuido;
int socketCentral;
struct sockaddr_in endDistribuido;
struct sockaddr_in endCentral;
unsigned int tamEndereco;
char *r_buffer, *s_buffer;
extern int n_pessoas; 
extern float temperatura;
extern float umidade;

void initSocket();
void *listenSocket_T();
void *listenSocket_1();
void *notificacao_T();
void *notificacao_1();

void handle_notificacao(char notificacao[]);

typedef struct{
    int estado;
} dispositivo;

typedef struct{
    dispositivo LS_T01;
    dispositivo LS_T02;
    dispositivo LC_T;
    dispositivo AC_T;
    dispositivo ASP;
    dispositivo SP_T;
    dispositivo SF_T;
    dispositivo SJ_T01;
    dispositivo SJ_T02;
    dispositivo SPo_T;
    dispositivo SC_IN;
    dispositivo SC_OUT;    
} andar_terreo;

andar_terreo dispositivos_T;

typedef struct{
    dispositivo LS_101;
    dispositivo LS_102;
    dispositivo LC_1;
    dispositivo AC_1;
    dispositivo SP_1;
    dispositivo SF_1;
    dispositivo SJ_1;
    dispositivo SJ_2;
} andar_1;

andar_1 dispositivos_1;
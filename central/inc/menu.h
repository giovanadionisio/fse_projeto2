#include <ncurses.h>
#include <curses.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>


#define LINE_START 1
#define SCAN_LINE 8
#define SCAN_COL 30

int yMax, xMax;
int option;

WINDOW *logs_gerais;
WINDOW *logs_terreo;
WINDOW *logs_1;
WINDOW *menu;

typedef struct{
    int estado;
} dispositivo;

typedef struct{
    dispositivo LS_T01;
    dispositivo LS_T02;
    dispositivo LC_T;
    dispositivo AC_T;
    dispositivo ASP;
    dispositivo SP_T;
    dispositivo SF_T;
    dispositivo SJ_T01;
    dispositivo SJ_T02;
    dispositivo SPo_T;
    dispositivo SC_IN;
    dispositivo SC_OUT;
    float temp;
    float umi;
    int n_pessoas;     
} andar_terreo;

andar_terreo dispositivos_terreo;

typedef struct{
    dispositivo LS_101;
    dispositivo LS_102;
    dispositivo LC_1;
    dispositivo AC_1;
    dispositivo SP_1;
    dispositivo SF_1;
    dispositivo SJ_1;
    dispositivo SJ_2;
    float temp;
    float umi;
} andar_1;

andar_1 dispositivos_1;
char *r_buffer_terreo, *s_buffer_terreo;
char *r_buffer_1, *s_buffer_1;

int p_solicitacao;
char solicitacao[1];
char id_dispositivo[1];
int estado_alarme_seguranca;
int alarme_seguranca;
int estado_alarme_incendio;

void iniciar_menu();
void *mostrar_logs_gerais();
void *mostrar_logs_terreo();
void *mostrar_logs_1();
void *mostrar_menu();
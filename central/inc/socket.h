#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include "../inc/menu.h"

#define PORTA_CENTRAL 10013
#define PORTA_TERREO 10113
#define PORTA_ANDAR1 10213
#define IP_DISTRIBUIDO "192.168.0.52"
#define NUMBER_QUEUE 20

struct sockaddr_in endTerreo, end1;
int socket_terreo, socket_1;
unsigned int tamanhoMensagem;

int bytesRecebidos;
int totalBytesRecebidos;
int totalBytesReceived;
int bytesReceived;
extern struct pinState state;


void *conectaTerreo();
void *conecta1();
void *envia_mensagens_terreo();
void *envia_mensagens_1();
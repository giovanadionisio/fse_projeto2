#include "../inc/main.h"

void tratasinal(int s){
    if (s == SIGPIPE || s == SIGINT){
        exit(0);
    }
}

void iniciar_servidor(){
    printf("Iniciando...\n\n");
    pthread_t t_conecta_terreo, t_conecta_1;

    pthread_create(&t_conecta_terreo, NULL, &conectaTerreo, NULL);
    pthread_create(&t_conecta_1, NULL, &conecta1, NULL);

    pthread_join(t_conecta_terreo, NULL);
    pthread_join(t_conecta_1, NULL);

    abrirCSV();

    iniciar_menu();
}

int main(){
    //signal(SIGPIPE, tratasinal);
    //signal(SIGINT, tratasinal);

    iniciar_servidor();

    pthread_create(&comunicacao_terreo, NULL, &envia_mensagens_terreo, NULL);
    pthread_create(&comunicacao_1, NULL, &envia_mensagens_1, NULL);
    pthread_create(&t_monitora_corredor, NULL, &monitora_corredor, NULL);
    pthread_create(&t_monitora_incendio, NULL, &monitora_incendio, NULL);
    pthread_create(&t_monitora_entradas, NULL, &monitora_entradas, NULL);
    pthread_create(&t_logs_gerais, NULL, &mostrar_logs_gerais, NULL);
    pthread_create(&t_logs_terreo, NULL, &mostrar_logs_terreo, NULL);
    pthread_create(&t_logs_1, NULL, &mostrar_logs_1, NULL);
    pthread_create(&t_menu, NULL, &mostrar_menu, NULL);
    
    pthread_join(t_logs_gerais, NULL);
    pthread_join(t_logs_terreo, NULL);
    pthread_join(t_logs_1, NULL);
    pthread_join(t_menu, NULL);
    pthread_join(comunicacao_terreo, NULL);
    pthread_join(comunicacao_1, NULL);

    
    //pthread_t t_logs_terreo, t_logs_1, t_menu;
    //pthread_create(&t_logs_terreo, NULL, &mostrar_logs_terreo, NULL);
    //pthread_create(&t_logs_1, NULL, &mostrar_logs_1, NULL);
    //pthread_create(&t_menu, NULL, &mostrar_menu, NULL);
    //pthread_join(t_logs_terreo, NULL);
    //pthread_join(t_logs_1, NULL);
    //pthread_join(t_menu, NULL);
    
    return 0;
}
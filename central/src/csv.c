#include "../inc/csv.h"
void abrirCSV() {
    FILE* arq;

    arq = fopen("logs/logs.csv", "a+");

    fseek(arq, 0, SEEK_END);
    if(ftell(arq) - 1 <= 0){
        fprintf(arq, "timestamp, horario, comando\n");
    }  
    
    fclose(arq);
}

void adicionar_linha(char dispositivo[], int comando) {
    FILE* arq;
    time_t horario;
    struct tm *timestamp;
    char buffer[80];
    

    arq = fopen("logs/logs.csv", "a+");

    time(&horario);
    timestamp = localtime(&horario);

    strftime(buffer,80,"%F %X",timestamp);

    if(comando){
        fprintf(arq, "%s, %s, %s\n", buffer, dispositivo, "ON");
    } else {
        fprintf(arq, "%s, %s, %s\n", buffer, dispositivo, "OFF");
    }

    
    

    fclose(arq);
}

#include "../inc/menu.h"

void iniciar_menu(){
    solicitacao[0] = 0;
    p_solicitacao = 0;
    alarme_seguranca = 0;
    
    estado_alarme_seguranca = 0;
    estado_alarme_incendio = 0;
    
    
    
    initscr();
    curs_set(0);
    
    getmaxyx(stdscr, yMax, xMax);
    
    logs_gerais = newwin(5, xMax, 0, 0);
    logs_terreo = newwin(9, xMax, 5, 0);
    logs_1 = newwin(9, xMax, 14, 0);
    menu = newwin(10, xMax, 23, 0);
    box(logs_gerais, 0, 0);
}

void *mostrar_logs_gerais(){
    while(1){
        sleep(1);
        mvwprintw(logs_gerais, LINE_START, (int) (xMax / 2) - 3, "GERAIS");
        mvwprintw(logs_gerais, LINE_START + 2, 5,  "Pessoas no predio: %d", dispositivos_terreo.n_pessoas);
        if(alarme_seguranca){
            mvwprintw(logs_gerais, LINE_START + 2, 35, "Alarme de Seguranca: ON ");
            if (estado_alarme_seguranca){
                mvwprintw(logs_gerais, LINE_START + 2, 67, "Alarme Seguranca: SOANDO   ");
            } else {
                mvwprintw(logs_gerais, LINE_START + 2, 67, "Alarme Seguranca: DESLIGADO");
            }
        } else {
            mvwprintw(logs_gerais, LINE_START + 2, 35, "Alarme de Seguranca: OFF");
        }
        if(estado_alarme_incendio){
            mvwprintw(logs_gerais, LINE_START + 2, 96, "Alarme Incendio: SOANDO   ");
        } else {
            mvwprintw(logs_gerais, LINE_START + 2, 96, "Alarme Incendio: DESLIGADO");
        }
        
        wrefresh(logs_gerais);
    }
}

void *mostrar_logs_terreo(){
    while(1){
        sleep(1);
        wclear(logs_terreo);
        box(logs_terreo, 0, 0);
        mvwprintw(logs_terreo, LINE_START, (int) (xMax / 2) - 10, "ESTADOS ANDAR TERREO");
        mvwprintw(logs_terreo, LINE_START + 1, 5, ""); 
        if(dispositivos_terreo.LS_T01.estado){
            mvwprintw(logs_terreo, LINE_START + 2, 5, "Lampada da Sala 01: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 2, 5, "Lampada da Sala 01: OFF");
        }

        if(dispositivos_terreo.LS_T02.estado){
            mvwprintw(logs_terreo, LINE_START + 2, 50, "Lampada da Sala 02: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 2, 50, "Lampada da Sala 02: OFF");
        }

        if(dispositivos_terreo.LC_T.estado){
            mvwprintw(logs_terreo, LINE_START + 2, 95, "Lampada do Corredor: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 2, 95, "Lampada do Corredor: OFF");
        }

        if(dispositivos_terreo.AC_T.estado){
            mvwprintw(logs_terreo, LINE_START + 3, 5, "Ar-Condicionado: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 3, 5, "Ar-Condicionado: OFF");
        }

        if(dispositivos_terreo.ASP.estado){
            mvwprintw(logs_terreo, LINE_START + 3, 50, "Aspessores: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 3, 50, "Aspessores: OFF");
        }

        if(dispositivos_terreo.SF_T.estado){
            mvwprintw(logs_terreo, LINE_START + 3, 95, "Sensor de Fumaca: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 3, 95, "Sensor de Fumaca: OFF");
        }

        if(dispositivos_terreo.SP_T.estado){
            mvwprintw(logs_terreo, LINE_START + 4, 5, "Sensor de Presenca: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 4, 5, "Sensor de Presenca: OFF");
        }

        if(dispositivos_terreo.SJ_T01.estado){
            mvwprintw(logs_terreo, LINE_START + 4, 50, "Sensor da Janela 1: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 4, 50, "Sensor da Janela 1: OFF");
        }

        if(dispositivos_terreo.SJ_T02.estado){
            mvwprintw(logs_terreo, LINE_START + 4, 95, "Sensor da Janela 2: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 4, 95, "Sensor da Janela 2: OFF");
        }

        if(dispositivos_terreo.SPo_T.estado){
            mvwprintw(logs_terreo, LINE_START + 5, 5, "Sensor da Porta: ON ");
        } else {
            mvwprintw(logs_terreo, LINE_START + 5, 5, "Sensor da Porta: OFF");
        }

        mvwprintw(logs_terreo, LINE_START + 5, 50, "Temperatura: %.2f °C", dispositivos_terreo.temp);

        mvwprintw(logs_terreo, LINE_START + 5, 95, "Umidade: %.2f\%", dispositivos_terreo.umi);
        //mvwprintw(logs_terreo, LINE_START + 2, 5, "%d %d %d %d %d %d %d %d %d %d %d %.2f %.2f %d\n", dispositivos_terreo.LS_T02.estado, 
        //dispositivos_terreo.LC_T.estado, dispositivos_terreo.AC_T.estado, dispositivos_terreo.ASP.estado, dispositivos_terreo.SP_T.estado, 
        //dispositivos_terreo.SF_T.estado, dispositivos_terreo.SJ_T01.estado, dispositivos_terreo.SJ_T02.estado, dispositivos_terreo.SPo_T.estado, 
        //dispositivos_terreo.SC_IN.estado,  dispositivos_terreo.SC_OUT.estado, dispositivos_terreo.temp, dispositivos_terreo.umi, 
        //dispositivos_terreo.n_pessoas); 
        wrefresh(logs_terreo);
    }
}

void *mostrar_logs_1(){
    while(1){
        sleep(1);
        wclear(logs_1);
        box(logs_1, 0, 0);
        mvwprintw(logs_1, LINE_START, (int) (xMax / 2) - 10, "ESTADOS PRIMEIRO ANDAR");
        mvwprintw(logs_1, LINE_START + 1, 5, ""); 
        if(dispositivos_1.LS_101.estado){
            mvwprintw(logs_1, LINE_START + 2, 5, "Lampada da Sala 01: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 2, 5, "Lampada da Sala 01: OFF");
        }

        if(dispositivos_1.LS_102.estado){
            mvwprintw(logs_1, LINE_START + 2, 50, "Lampada da Sala 02: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 2, 50, "Lampada da Sala 02: OFF");
        }

        if(dispositivos_1.LC_1.estado){
            mvwprintw(logs_1, LINE_START + 2, 95, "Lampada do Corredor: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 2, 95, "Lampada do Corredor: OFF");
        }

        if(dispositivos_1.AC_1.estado){
            mvwprintw(logs_1, LINE_START + 3, 5, "Ar-Condicionado: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 3, 5, "Ar-Condicionado: OFF");
        }

        if(dispositivos_1.SF_1.estado){
            mvwprintw(logs_1, LINE_START + 3, 50, "Sensor de Fumaca: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 3, 50, "Sensor de Fumaca: OFF");
        }

        if(dispositivos_1.SP_1.estado){
            mvwprintw(logs_1, LINE_START + 3, 95, "Sensor de Presenca: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 3, 95, "Sensor de Presenca: OFF");
        }

        if(dispositivos_1.SJ_1.estado){
            mvwprintw(logs_1, LINE_START + 3, 5, "Sensor da Janela 1: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 3, 5, "Sensor da Janela 1: OFF");
        }

        if(dispositivos_1.SJ_2.estado){
            mvwprintw(logs_1, LINE_START + 3, 50, "Sensor da Janela 2: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 3, 50, "Sensor da Janela 2: OFF");
        }

        if(dispositivos_1.SP_1.estado){
            mvwprintw(logs_1, LINE_START + 3, 95, "Sensor de Presenca: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 3, 95, "Sensor de Presenca: OFF");
        }

        if(dispositivos_1.SF_1.estado){
            mvwprintw(logs_1, LINE_START + 4, 5, "Sensor de Fumaca: ON ");
        } else {
            mvwprintw(logs_1, LINE_START + 4, 5, "Sensor de Fumaca: OFF");
        }

        mvwprintw(logs_1, LINE_START + 4, 50, "Temperatura: %.2f °C", dispositivos_1.temp);

        mvwprintw(logs_1, LINE_START + 4, 95, "Umidade: %.2f\%", dispositivos_1.umi);

        //mvwprintw(logs_1, LINE_START + 2, 5, "%d %d %d %d %d %d %d %d %.2f %.2f\n", dispositivos_1.LS_101.estado, dispositivos_1.LS_102.estado, dispositivos_1.LC_1.estado, 
        //dispositivos_1.AC_1.estado, dispositivos_1.SP_1.estado, dispositivos_1.SF_1.estado, dispositivos_1.SJ_1.estado, dispositivos_1.SJ_2.estado, dispositivos_1.temp, dispositivos_1.umi); 
        wrefresh(logs_1);
    }
}

void mostrar_menu_terreo(){
    while(1){
        box(menu, 0, 0);

        mvwprintw(menu, LINE_START, (int) xMax / 2, "MENU TERREO");
        mvwprintw(menu, LINE_START + 1, 5, "");
        if(dispositivos_terreo.LC_T.estado){
            mvwprintw(menu, LINE_START + 2, 5, "1 - Desligar Lampada Corredor");
        } else {
            mvwprintw(menu, LINE_START + 2, 5, "1 - Ligar Lampada Corredor");
        }

        if(dispositivos_terreo.LS_T01.estado){
            mvwprintw(menu, LINE_START + 2, 50, "2 - Desligar Lampada da Sala 01");
        } else {
            mvwprintw(menu, LINE_START + 2, 50, "2 - Ligar Lampada da Sala 01");
        }

        if(dispositivos_terreo.LS_T02.estado){
            mvwprintw(menu, LINE_START + 3, 5, "3 - Desligar Lampada da Sala 02");
        } else {
            mvwprintw(menu, LINE_START + 3, 5, "3 - Ligar Lampada da Sala 02");
        }

        if(dispositivos_terreo.AC_T.estado){
            mvwprintw(menu, LINE_START + 3, 50, "4 - Desligar Ar Condicionado");
        } else {
            mvwprintw(menu, LINE_START + 3, 50, "4 - Ligar Ar Condicionado");
        }
    
        mvwprintw(menu, LINE_START + 5, 5, "0 - Voltar");

        wrefresh(menu);

        mvwscanw(menu, SCAN_LINE, SCAN_COL, "%d", &option);
        switch (option){
        case 0:
            return;
            break;
        case 1:
            adicionar_linha("LC_T", !dispositivos_terreo.LC_T.estado);
            p_solicitacao = 1; 
            solicitacao[0] = 1;
            id_dispositivo[0] = 27;
            return;
        case 2:
            adicionar_linha("LS_T01", !dispositivos_terreo.LS_T01.estado);
            p_solicitacao = 1; 
            solicitacao[0] = 1;
            id_dispositivo[0] = 4;
            return;
        case 3:
            adicionar_linha("LS_T02", !dispositivos_terreo.LS_T02.estado);
            p_solicitacao = 1; 
            solicitacao[0] = 1;
            id_dispositivo[0] = 17;
            return;
        case 4:
            adicionar_linha("AC_T", !dispositivos_terreo.AC_T.estado);
            p_solicitacao = 1; 
            solicitacao[0] = 1;
            id_dispositivo[0] = 7;
            return;
        default:
            break;
        }

        wclear(menu);
    }
}

void mostrar_menu_1(){
while(1){
        box(menu, 0, 0);
        mvwprintw(menu, LINE_START, (int) xMax / 2, "MENU PRIMEIRO ANDAR");
        mvwprintw(menu, LINE_START + 1, 5, "");
        if(dispositivos_1.LC_1.estado){
            mvwprintw(menu, LINE_START + 2, 5, "1 - Desligar Lampada Corredor");
        } else {
            mvwprintw(menu, LINE_START + 2, 5, "1 - Ligar Lampada Corredor");
        }

        if(dispositivos_1.LS_101.estado){
            mvwprintw(menu, LINE_START + 2, 50, "2 - Desligar Lampada da Sala 01");
        } else {
            mvwprintw(menu, LINE_START + 2, 50, "2 - Ligar Lampada da Sala 01");
        }

        if(dispositivos_1.LS_102.estado){
            mvwprintw(menu, LINE_START + 3, 5, "3 - Desligar Lampada da Sala 02");
        } else {
            mvwprintw(menu, LINE_START + 3, 5, "3 - Ligar Lampada da Sala 02");
        }

        if(dispositivos_1.AC_1.estado){
            mvwprintw(menu, LINE_START + 3, 50, "4 - Desligar Ar Condicionado");
        } else {
            mvwprintw(menu, LINE_START + 3, 50, "4 - Ligar Ar Condicionado");
        }

        mvwprintw(menu, LINE_START + 5, 5, "0 - Voltar");
        
        wrefresh(menu);

        mvwscanw(menu, SCAN_LINE, SCAN_COL, "%d", &option);
        switch (option){
        case 0:
            return;
            break;
        case 1:
            adicionar_linha("LC_1", !dispositivos_1.LC_1.estado);
            p_solicitacao = 2; 
            solicitacao[0] = 1;
            id_dispositivo[0] = 8;
            return;
        case 2:
            adicionar_linha("LS_101", !dispositivos_1.LS_101.estado);
            p_solicitacao = 2; 
            solicitacao[0] = 1;
            id_dispositivo[0] = 22;
            return;
        case 3:
            adicionar_linha("LS_102", !dispositivos_1.LS_102.estado);
            p_solicitacao = 2; 
            solicitacao[0] = 1;
            id_dispositivo[0] = 25;
            return;
        case 4:
            adicionar_linha("AC_1", !dispositivos_1.AC_1.estado);
            p_solicitacao = 2; 
            solicitacao[0] = 1;
            id_dispositivo[0] = 12;
            return;
        default:
            break;
        }

        wclear(menu);
    }
}

void mostrar_menu_alarmes(){
while(1){
        box(menu, 0, 0);
        mvwprintw(menu, LINE_START, (int) xMax / 2, "MENU ALARMES");
        mvwprintw(menu, LINE_START + 1, 5, "");
        if(alarme_seguranca){
            mvwprintw(menu, LINE_START + 2, 5, "1 - Desligar Alarme de Seguranca");
        } else {
            mvwprintw(menu, LINE_START + 2, 5, "1 - Ligar Alarme de Seguranca");
        }

        mvwprintw(menu, LINE_START + 5, 5, "0 - Voltar");
        
        wrefresh(menu);

        mvwscanw(menu, SCAN_LINE, SCAN_COL, "%d", &option);
        switch (option){
        case 0:
            return;
            break;
        case 1:
            adicionar_linha("ALARME_SEGURANCA", !alarme_seguranca);
            alarme_seguranca = !alarme_seguranca;
            return;
        default:
            break;
        }

        wclear(menu);
    }
}

void *mostrar_menu(){
    while(1){
        wclear(menu);
        box(menu, 0, 0);
        mvwprintw(menu, LINE_START, (int) xMax / 2, "MENU");
        mvwprintw(menu, LINE_START + 1, 5, "");
        mvwprintw(menu, LINE_START + 2, 5, "1 - Solicitacao para o andar terreo");
        mvwprintw(menu, LINE_START + 3, 5, "2 - Solicitacao para o primeiro andar"); 
        mvwprintw(menu, LINE_START + 4, 5, "3 - Gerenciar Alarme"); 
        mvwprintw(menu, LINE_START + 5, 5, ""); 
        mvwprintw(menu, LINE_START + 6, 5, "0 - Finalizar programa"); 
        wrefresh(menu);

        mvwscanw(menu, SCAN_LINE, SCAN_COL, "%d", &option);
        wclear(menu);

        if(option == 1){
            mostrar_menu_terreo();
        } else if (option == 2){
            mostrar_menu_1();
        } else if (option == 3){
            mostrar_menu_alarmes();
        } else if (option == 0){
            kill(getpid(), SIGINT);
        }
    }
}
#include "../inc/socket.h"

void *conectaTerreo() {
    // create a Socket 
    if ((socket_terreo = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        printf("Error when start socket. Try again\n");
        exit(1);
    } 

    // Build struct sockaddr in
    memset(&endTerreo, 0, sizeof(endTerreo));
    endTerreo.sin_family = AF_INET;
    endTerreo.sin_addr.s_addr = inet_addr(IP_DISTRIBUIDO);
    endTerreo.sin_port = htons(PORTA_TERREO);

    // connect server
    if (connect(socket_terreo, (struct sockaddr *) &endTerreo, sizeof(endTerreo)) < 0){
        printf("Error when connect with server Terreo\n");
        exit(1);
    } else {
        printf("OK\n");
    }
}

void *conecta1() {
    // create a Socket 
    if ((socket_1 = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        printf("Error when start socket. Try again\n");
        exit(1);
    }

    // Build struct sockaddr in
    memset(&end1, 0, sizeof(end1));
    end1.sin_family = AF_INET;
    end1.sin_addr.s_addr = inet_addr(IP_DISTRIBUIDO);
    end1.sin_port = htons(PORTA_ANDAR1);

    // connect server
    if (connect(socket_1, (struct sockaddr *) &end1, sizeof(end1)) < 0){
        printf("Error when connect with server Primeiro Andar\n");
        exit(1);
    } else {
        printf("OK\n");
    }
}

void carrega_dados_terreo(){
    dispositivos_terreo.LS_T01.estado = r_buffer_terreo[0];
    dispositivos_terreo.LS_T02.estado = r_buffer_terreo[1];
    dispositivos_terreo.LC_T.estado = r_buffer_terreo[2];
    dispositivos_terreo.AC_T.estado = r_buffer_terreo[3];
    dispositivos_terreo.ASP.estado = r_buffer_terreo[4];
    dispositivos_terreo.SP_T.estado = r_buffer_terreo[5];
    dispositivos_terreo.SF_T.estado = r_buffer_terreo[6];
    dispositivos_terreo.SJ_T01.estado = r_buffer_terreo[7];
    dispositivos_terreo.SJ_T02.estado = r_buffer_terreo[8];
    dispositivos_terreo.SPo_T.estado = r_buffer_terreo[9];
    dispositivos_terreo.SC_IN.estado = r_buffer_terreo[10];
    dispositivos_terreo.SC_OUT.estado = r_buffer_terreo[11];
    memcpy(&dispositivos_terreo.temp, &r_buffer_terreo[12], 4);
    memcpy(&dispositivos_terreo.umi, &r_buffer_terreo[16], 4);
    dispositivos_terreo.n_pessoas = r_buffer_terreo[20];
}

void mensagem_terreo(){
    s_buffer_terreo[0] = dispositivos_terreo.LS_T01.estado;
    s_buffer_terreo[1] = dispositivos_terreo.LS_T02.estado;
    s_buffer_terreo[2] = dispositivos_terreo.LC_T.estado;
    s_buffer_terreo[3] = dispositivos_terreo.AC_T.estado;
    s_buffer_terreo[4] = dispositivos_terreo.ASP.estado;
    s_buffer_terreo[5] = dispositivos_terreo.SP_T.estado;
    s_buffer_terreo[6] = dispositivos_terreo.SF_T.estado;
    s_buffer_terreo[7] = dispositivos_terreo.SJ_T01.estado;
    s_buffer_terreo[8] = dispositivos_terreo.SJ_T02.estado;
    s_buffer_terreo[9] = dispositivos_terreo.SPo_T.estado;
    s_buffer_terreo[10] = dispositivos_terreo.SC_IN.estado;
    s_buffer_terreo[11] = dispositivos_terreo.SC_OUT.estado;
    memcpy(&s_buffer_terreo[12], &dispositivos_terreo.temp, 4);
    memcpy(&s_buffer_terreo[16], &dispositivos_terreo.umi, 4);
    s_buffer_terreo[20] = dispositivos_terreo.n_pessoas;
}

void carrega_dados_1(){
    dispositivos_1.LS_101.estado = r_buffer_1[0];
    dispositivos_1.LS_102.estado = r_buffer_1[1];
    dispositivos_1.LC_1.estado = r_buffer_1[2];
    dispositivos_1.AC_1.estado = r_buffer_1[3];
    dispositivos_1.SP_1.estado = r_buffer_1[4];
    dispositivos_1.SF_1.estado = r_buffer_1[5];
    dispositivos_1.SJ_1.estado = r_buffer_1[6];
    dispositivos_1.SJ_2.estado = r_buffer_1[7];
    memcpy(&dispositivos_1.temp, &r_buffer_1[8], 4);
    memcpy(&dispositivos_1.umi, &r_buffer_1[12], 4);
}

void mensagem_1(){
    s_buffer_1[0] = dispositivos_1.LS_101.estado;
    s_buffer_1[1] = dispositivos_1.LS_102.estado;
    s_buffer_1[2] = dispositivos_1.LC_1.estado;
    s_buffer_1[3] = dispositivos_1.AC_1.estado;
    s_buffer_1[4] = dispositivos_1.SP_1.estado;
    s_buffer_1[5] = dispositivos_1.SF_1.estado;
    s_buffer_1[6] = dispositivos_1.SJ_1.estado;
    s_buffer_1[7] = dispositivos_1.SJ_2.estado;
    memcpy(&s_buffer_1[8], &dispositivos_1.temp, 4);
    memcpy(&s_buffer_1[12], &dispositivos_1.umi, 4);
}

void enviamsg_terreo(){
    mensagem_terreo();
    send(socket_terreo, solicitacao, 1, 0);
    if(solicitacao[0] && (p_solicitacao == 1 || p_solicitacao == 3)){
        solicitacao[0] = 0;
        p_solicitacao = 0;
        send(socket_terreo, id_dispositivo, 1, 0);
    } 
    send(socket_terreo, s_buffer_terreo, 21, 0);
    read(socket_terreo, r_buffer_terreo, 21);
    carrega_dados_terreo();
}

void enviamsg_1(){
    mensagem_1();
    send(socket_1, solicitacao, 1, 0);
    if(solicitacao[0] && (p_solicitacao == 2 || p_solicitacao == 3)){
        solicitacao[0] = 0;
        p_solicitacao = 0;
        send(socket_1, id_dispositivo, 1, 0);
    } 
    send(socket_1, s_buffer_1, 16, 0);
    read(socket_1, r_buffer_1, 16);
    carrega_dados_1();
}

void *envia_mensagens_terreo() {
    r_buffer_terreo = malloc(sizeof(char) * 21);
    //memset(&r_buffer_terreo, 0, 21);
    s_buffer_terreo = malloc(sizeof(char) * 21);
    //memset(&r_buffer_1, 0, 21);

    while (1)
    {
        enviamsg_terreo();
        sleep(1);
    }
}

void *envia_mensagens_1() {
    r_buffer_1 = malloc(sizeof(char) * 16);
    //memset(&r_buffer_1, 0, 16);
    s_buffer_1 = malloc(sizeof(char) * 16);
    //memset(&s_buffer_1, 0, 16);

    while (1)
    {
        enviamsg_1();
        sleep(1);
    }
}
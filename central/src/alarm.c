#include "../inc/alarm.h"

void *monitora_corredor(){
    while(1){
        if(dispositivos_terreo.SP_T.estado || dispositivos_1.SP_1.estado){
            if(!alarme_seguranca){
                p_solicitacao = 3;
                if(dispositivos_terreo.SP_T.estado){
                    id_dispositivo[0] = 50;
                    p_solicitacao = 3;
                    solicitacao[0] = 1;
                    sleep(10);
                    id_dispositivo[0] = 51;
                    p_solicitacao = 3;
                    solicitacao[0] = 1;
                } else {
                    id_dispositivo[0] = 52;
                    p_solicitacao = 3;
                    solicitacao[0] = 1;
                    sleep(10);
                    id_dispositivo[0] = 53;
                    p_solicitacao = 3;
                    solicitacao[0] = 1;
                }
            } else {
                adicionar_linha("SOM_ALARME_SEGURANCA", 1);
                while(dispositivos_terreo.SP_T.estado || dispositivos_1.SP_1.estado){
                    estado_alarme_seguranca = 1;
                    sleep(1);
                }
                adicionar_linha("SOM_ALARME_SEGURANCA", 0);
                estado_alarme_seguranca = 0;
            }
        }
    }
}

void *monitora_incendio(){
    while(1){
        if(dispositivos_terreo.SF_T.estado || dispositivos_1.SF_1.estado){
        estado_alarme_incendio = 1;
        id_dispositivo[0] = 60;
        p_solicitacao = 3;
        solicitacao[0] = 1;
        adicionar_linha("SOM_ALARME_INCENDIO", 1);
        while(dispositivos_terreo.SF_T.estado || dispositivos_1.SF_1.estado){
            sleep(1);
        }
        adicionar_linha("SOM_ALARME_INCENDIO", 0);
        estado_alarme_incendio = 0;
        id_dispositivo[0] = 61;
        p_solicitacao = 3;
        solicitacao[0] = 1;
        }
    }
}

void *monitora_entradas(){
    int aux = 0;
    while(1){
        if(alarme_seguranca){
            while(dispositivos_terreo.SJ_T01.estado || dispositivos_terreo.SJ_T02.estado || dispositivos_terreo.SPo_T.estado 
            || dispositivos_1.SJ_1.estado || dispositivos_1.SJ_2.estado){
                if(!aux) {
                    aux = 1;
                    adicionar_linha("SOM_ALARME_SEGURANCA", 1);
                }    
                estado_alarme_seguranca == 1;
                system("omxplayer assets/alarme.mp3 > /dev/null");
            }
            if(aux){
                adicionar_linha("SOM_ALARME_SEGURANCA", 0);
            }
            aux = 0;
        }
    }
}